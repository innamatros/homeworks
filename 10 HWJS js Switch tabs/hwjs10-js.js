const tabsTitleList = Array.from(document.querySelectorAll(".tabs-title"));
const tabsContentList = Array.from(document.querySelectorAll(".content-title"));

const removeActiveClass = (element, className = "active") => {
  element.forEach(item => {
    if (item.classList.contains(`${className}`)) {
      item.classList.remove(`${className}`)
    }
  })
};
const setActiveClass = (element, className = "active") => {
  element.classList.add(`${className}`)
};
const pointedTab = (item) => {
  item.addEventListener("click", () => {

    removeActiveClass(tabsTitleList);
    removeActiveClass(tabsContentList);

    const activeTabcontent = tabsContentList.find(listItem => {
      return listItem.dataset.tab === item.dataset.tab
    }
    )

    setActiveClass(item);
    setActiveClass(activeTabcontent);
  })
}

tabsTitleList.forEach(pointedTab)

