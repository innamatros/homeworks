function CreateNewUser() {
   this.getLogin = function () {
    return (this.firstName[0].toLowerCase() +this.lastName.toLowerCase());
  }
  Object.defineProperty(this, "firstName", {
    get: function getFirstName() { return this._firstName; },
    set: function setFirstName(value) { this._firstName = value }
  });

  Object.defineProperty(this, "lastName", {
    get: function getLastName() { return this._lastName; },
    set: function setLastName(value) { this._lastName = value }
  });
  this.firstName = prompt("Enter your first name!");
  this.lastName = prompt("Enter your last name!");
} 

const newUser = new CreateNewUser("firstName", "lastName")

console.log(newUser.getLogin());

