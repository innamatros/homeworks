1. Які існують типи даних у Javascript?

В JS виділяють 8 типів даних, а саме:
1. Number - число.
2. String - рядок.
3. Boolean - логічний, який має два значення true або false.
4. Null - порожнє.
5. Undefined - значення не писвоїли.
6. Object - об'єкти.
7. BigInt - велике ціле число.
8. Typeof - показує тип аргументу.


2. У чому різниця між == і ===?

== порівнює два значення, але не бере до уваги тип.
=== також порівнює два значення, але із урахуванням вже типу.


3. Що таке оператор?

За допомогою операторів в JS можна здійснювати арифметичні дії над числами.
Основними операторами є - +, -, *, /, %, >, <, >=, <=. Тобто оператор визначає, яка дія буде виконуватися між числами.