let firstName = prompt("Enter your name!");
while (firstName == "") {
  firstName = prompt("You haven't entered the name. Please, try again!");
}
alert("You entered " + firstName);

let age = Number(prompt("Enter your age!"));
while (Number.isNaN(age)) {
  age = Number(prompt("You haven't entered the age. Please, try again!"));
}
alert("You entered " + age);

if (age < 18) {
  alert("You are not allowed to visit this website.");
}
else if (age >= 18 && age <= 22) {
  let accessAllowed = confirm("Are you sure you want to continue?");
  if (accessAllowed == Boolean(true))
    alert("Welcome, "  + firstName);
  else
    alert("You are not allowed to visit this website.");
}
else {
  alert("Welcome, "  + firstName);
}
