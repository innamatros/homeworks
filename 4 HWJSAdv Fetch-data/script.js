const URL = 'https://ajax.test-danit.com/api/swapi/';
const root = document.querySelector('#root');

class Requests {
    constructor(url) {
        this.url = url;
    }

    getEntity(entity) {
        return fetch(this.url + `${entity}`).then(response => {
            return response.json();
        });
    }
    getDeepEntity(entity, keyword) {
     return fetch(this.url + `${entity}?search=${keyword}`).then(response => {
         return response.json();
     });
    }
}

class StarWars {
    constructor(root) {
        this.root = root;
    }

    renderEntity(data) {
        const infoEntity = document.createElement('div');

        const infoItems = data.map(({ name, episodeId, openingCrawl, characters }) => {
            const entityContainer = document.createElement('div');
            const title = document.createElement('h4');
            title.textContent = `Film name: "${name}"`;
          
            
          
            const id = document.createElement('p');
            id.textContent = `Episode: ${episodeId}`;
            id.style.fontStyle = 'italic';
         const namesCharacter = document.createElement("p");
            characters.forEach(elem => {
                new Requests(elem).getEntity("").then(character => {namesCharacter.append(character.name)});
                });
                id.append(namesCharacter);
            const description = document.createElement('p');
            description.textContent = `Description:${openingCrawl}`;
            entityContainer.append(title, id, description);
            return entityContainer;
        });
        infoEntity.append(...infoItems);
        this.root.append(infoEntity);
    }
}


new Requests(URL).getEntity("films").then(data => new StarWars(root).renderEntity(data));
