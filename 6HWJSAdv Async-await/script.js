async function getUserIp() {
    try {
        const button = document.querySelector("button");
        const resultIp = await (await fetch('https://api.ipify.org/?format=json')).json();
        button.addEventListener("click", () => {
            return resultIp;
        });
        // console.log(resultIp);
        const resultAddress = await (await fetch(`http://ip-api.com/json/${resultIp.ip}?fields=status,message,continent,country,regionName,
        city,district`)).json();
        // console.log(resultAddress);
        const text = document.createElement("p");
        text.innerText = JSON.stringify(resultAddress);
        document.body.append(text);
        return resultAddress;
    }
    catch (err) {
        console.log('Error during IP address request, see below:');
        console.error(err);
    }
}
getUserIp();



// const URL = 'https://api.ipify.org/?format=json';
// const button = document.querySelector("button");
// button.addEventListener("click", () => {
//     new Requests(URL).getEntity().then((result) => {

//         new Requests(`http://ip-api.com/json/${result.ip}?fields=status,message,continent,country,regionName,city,district`)
//             .getEntity().then((result) => {
//                 const text = document.createElement("p");
//                 text.innerText = JSON.stringify(result);
//                 document.body.append(text);
//             });
//     });
// });
// // console.log(button);

// class Requests {
//     constructor(url) {
//         this.url = url;
//     }
//     getEntity(entity = "") {
//         return fetch(this.url + `${entity}`).then(response => {
//             return response.json();
//         });
//     }
// }
