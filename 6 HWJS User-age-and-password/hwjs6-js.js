function CreateNewUser() {
  this.getLogin = function () {
    return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
  }

  this.getAge = function () {
    let now = new Date();
    let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    this._birthDayNow = new Date(today.getFullYear(), this._birthDay.getMonth(), this._birthDay.getDate());
    if (today < this._birthDayNow) {
      return (today.getFullYear() - this.birthDay.getFullYear() - 1);
    } else {
      return (today.getFullYear() - this.birthDay.getFullYear());
    }
  };

  this.getPassword = function () {
    return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDay.getFullYear());
  }

  Object.defineProperty(this, "firstName", {
    get: function getFirstName() { return this._firstName; },
    set: function setFirstName(value) { this._firstName = value }
  });

  Object.defineProperty(this, "lastName", {
    get: function getLastName() { return this._lastName; },
    set: function setLastName(value) { this._lastName = value }
  });

  Object.defineProperty(this, "birthDay", {
    get: function getbirthDay() { return this._birthDay; },
    set: function (value) {
      let arrayBirthDay = value.split(".");
      this._birthDay = new Date(`${arrayBirthDay[2]}-${arrayBirthDay[1]}-${arrayBirthDay[0]}`);
    }
  });

  this.firstName = prompt("Enter your first name!");
  this.lastName = prompt("Enter your last name!");
  this.birthDay = prompt("Enter your birthday!", "dd.mm.yyyy");
}

const newUser = new CreateNewUser("firstName", "lastName", "birthDay");

console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
