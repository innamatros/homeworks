const inputPassword = document.getElementById("password");
const confirminputPassword = document.getElementById("confirm-password");
const iconEye = document.getElementById("eye-icon");

function changeElementsForm(icon) {
  icon.classList.toggle("fa-eye-slash")
  const labelParent = icon.closest(".input-wrapper");
  const input = labelParent.querySelector("input");
  const type = input.getAttribute("type") === "password" ? "text" : "password"
  input.setAttribute("type", type)
};

function confirmPassword() {
  if (inputPassword.value === confirminputPassword.value) {
    alert("You are welcome");
  } else {
    const errorMessage = document.querySelector(".error");
    if (errorMessage) {
      return;
    }
    let div = document.createElement("div");
    div.innerText = "Потрібно ввести однакові значення";
    div.classList.add("error");
    this.before(div);
  }
}

const form = document.querySelector("form");
form.addEventListener("click", function (event) {
  event.preventDefault();
  if (event.target.tagName === "I") {
    changeElementsForm(event.target);
  }
});
