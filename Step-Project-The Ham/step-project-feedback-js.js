let employeeInformations = document.querySelectorAll(".employee-info");
let navIcons = document.querySelectorAll(".photo-icon-container");
let currentInfo = 0;


document.querySelector(".arrow-right").addEventListener("click", () => {
  changeInfo(currentInfo + 1)
});
document.querySelector(".arrow-left").addEventListener("click", () => {
  changeInfo(currentInfo - 1)
});

function changeInfo(nextTo) {
  if (nextTo >= employeeInformations.length) { nextTo = 0; }
  if (nextTo < 0) { nextTo = employeeInformations.length - 1; }

  employeeInformations[currentInfo].classList.toggle("active");
  navIcons[currentInfo].classList.toggle("active");
  employeeInformations[nextTo].classList.toggle("active");
  navIcons[nextTo].classList.toggle("active");

  currentInfo = nextTo;
}

document.querySelectorAll(".photo-icon-container").forEach((bullet, bulletIndex) => {
  bullet.addEventListener("click", () => {
    if (currentInfo !== bulletIndex) {
      changeInfo(bulletIndex);
    }
  })
})



