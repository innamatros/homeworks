const list = document.querySelector(".work-list");
const items = document.querySelectorAll(".graphic-font-image");

function filter() {
  list.addEventListener("click", event => {
    const targetList = event.target.dataset.list
    switch (targetList) {
      case "all":
        getImages("graphic-font-image")
        break
      case "graphic":
        getImages(targetList)
        break
      case "web":
        getImages(targetList)
        break
      case "landing":
        getImages(targetList)
        break
      case "press":
        getImages(targetList)
        break
    }
  })
}
filter()

function getImages(className) {
  items.forEach(item => {
    if (item.classList.contains(className)) {
      item.style.display = "block"
    } else {
      item.style.display = "none"
    }
  })
}


const buttonLoad = document.querySelector(".button-load");
const images = document.querySelector(".hidden").length;
let currentimg = 0;

buttonLoad.addEventListener("click", () => {
  currentimg += 12;
  const array = Array.from(document.querySelectorAll(".hidden"));
  const visibleImages = array.slice(0, currentimg);
  visibleImages.forEach(element => element.classList.add("visible"));

  if (currentimg.length === images) {
    buttonLoad.style.display = "none";
  }
});