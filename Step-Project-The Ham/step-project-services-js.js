const serviceItemList = Array.from(document.querySelectorAll(".service-item"));
const serviceImageList = Array.from(document.querySelectorAll(".service-image"));
const serviceContentList = Array.from(document.querySelectorAll(".service-content"));


const removeActiveClass = (element, className = "active") => {
  element.forEach(item => {
    if (item.classList.contains(`${className}`)) {
      item.classList.remove(`${className}`)
    }
  })
};
const setActiveClass = (element, className = "active") => {
  element.classList.add(`${className}`)
};

const pointedItem = (item) => {
  item.addEventListener("click", () => {

    removeActiveClass(serviceItemList);
    removeActiveClass(serviceImageList);
    removeActiveClass(serviceContentList);
    const activeImageContent = serviceImageList.find(listItem => {
      return listItem.dataset.tab === item.dataset.tab
    }
    )
    const activeItemContent = serviceContentList.find(listItem => {
      return listItem.dataset.tab === item.dataset.tab
    }
    )
    setActiveClass(item);
    setActiveClass(activeImageContent);
    setActiveClass(activeItemContent);
  })
}
serviceItemList.forEach(pointedItem);
