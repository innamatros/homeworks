const button = document.querySelector(".change-button");
const theme = document.querySelector(".theme-item");

button.addEventListener("click", function() {
  if (theme.getAttribute("href") == "./hwjs14-css-white.css") {
    theme.href = "./hwjs14-css-dark.css";
    localStorage.setItem("theme", "./hwjs14-css-dark.css");
  } else {
    theme.href = "./hwjs14-css-white.css";
    localStorage.setItem("theme", "./hwjs14-css-white.css");
  }
});

function setTheme() {
    theme.href = localStorage.getItem("theme") || "./hwjs14-css-white.css"
  }
