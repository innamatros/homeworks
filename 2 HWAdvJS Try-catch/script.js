const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",

  },
];

console.log(books);
const KEY_IN_OBJ = ["author", "name", "price"];

function createList(books) {
  const list = document.createElement("ul");
  for (let i = 0; i < books.length; i++) {
    const listItem = document.createElement("li");
    const currentEl = books[i];
    try {
      let errorMessage = "";
      KEY_IN_OBJ.forEach((key) => {
        if (!currentEl[key]) {
          errorMessage += `${ key }`;
        }
      });

      if (!errorMessage) {
        listItem.textContent = JSON.stringify(currentEl);
        list.appendChild(listItem);
      } else {
        throw new Error("Не має таких даних:" + errorMessage);
      }
    } catch (error) {
      console.log(error.message);
    }
  }
  return list;
}

document.getElementById("root").appendChild(createList(books));

