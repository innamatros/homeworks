const images = document.querySelectorAll(".image-to-show");
let index = 0;
let imageInterval = setInterval(changeImage, 3000);

function changeImage() {
  for (let i = 0; i < images.length; i++) {
    images[i].style.display = "none";
  }
  index++
  if (index > images.length) {
    index = 1
  }
  images[index - 1].style.display = "block";
}
changeImage()

let show = true;

function pauseChangeImage() {
  show = false;
  clearInterval(imageInterval);;
}
function playChangeImage() {
  if (!show) {
    show = true;
    imageInterval = setInterval(changeImage, 3000);
  }
}

