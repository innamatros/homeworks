1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.

Функції setTimeout() і setInterval() мають однаковий синтаксис написання. Але функція 
setTimeout() запускається один раз, а setInterval() буде запускатися періодично через 
вказаний проміжок часу.


2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

Якщо у функцію setTimeout() передати нульову затримку, то функція запуститься одразу, як тільки
буде виконаний поточний код.


3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

Виклик функції clearInterval() дозволяє зупинити подальші виклики, які вже не потрібні, тим самим, не завантажуючи 
пам'ять.