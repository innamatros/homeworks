function getFirstNumber() {
  let firstnumber = prompt("Please, enter the first number.");
  console.log(firstnumber);
  while (firstnumber == "" || firstnumber == null || Number.isNaN(Number(firstnumber))) {
    firstnumber = Number(prompt("Please, enter the first number again."));
  }
  return firstnumber;
}


function getSecondNumber() {
  let secondnumber = prompt("Please, enter the second number.");
  console.log(secondnumber);
  while (secondnumber == "" || secondnumber == null || Number.isNaN(Number(secondnumber))) {
    secondnumber = Number(prompt("Please, enter the second number again."));
  }
  return secondnumber;
}


function getMathSymbol() {
  let symbol = prompt("Please, enter the math symbol.");
  console.log(symbol);
  while (
    symbol !== "+" &&
    symbol !== "-" &&
    symbol !== "/" &&
    symbol !== "*"
  ) {
    symbol = prompt("You need to enter + or - or * or /. Try again.");
  }
  return symbol;
}


function getResult() {
  do {
    let firstNum = Number(getFirstNumber());
    let secondNum = Number(getSecondNumber());
    let mathSymb = getMathSymbol();
    switch (mathSymb) {
      case "+":
        console.log(firstNum + secondNum);
        break;
      case "-":
        console.log(firstNum - secondNum);
        break;
      case "/":
        console.log(firstNum / secondNum);
        break;
      case "*":
        console.log(firstNum * secondNum);
        break;
    }
  } while (confirm("Do you want to continue?"));
  alert("Have a nice day!");
}
getResult();
