const dropmenuIcon = document.querySelector(".dropmenu__icon");
const navigationList = document.querySelector(".navigation__list");
dropmenuIcon.addEventListener("click", function () {
  if (dropmenuIcon.innerText === "menu") {
    dropmenuIcon.innerText = "close";
  } else {
    dropmenuIcon.innerText = "menu";
  }
  navigationList.classList.toggle("non-active");
});

