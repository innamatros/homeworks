class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
};

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get lang() {
    return this._lang;
  }
  set lang(value) {
    this._lang = value;
  }
  get salary() {
    return this._salary * 3;
  }
};

let programmer1 = new Programmer("Olena", 30, 1000, "eng");
console.log(programmer1);
// console.log(programmer1.salary);
let programmer2 = new Programmer("Anna", 35, 5000, "ukr");
console.log(programmer2);
let programmer3 = new Programmer("Kira", 40, 10000, "fr");
console.log(programmer3);

