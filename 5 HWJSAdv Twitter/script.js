const URL = "https://ajax.test-danit.com/api/json/";
const Post = "https://ajax.test-danit.com/api/json/posts/";

class Requests {
    constructor(url) {
        this.url = url;
    }
    delete(id) {
        return fetch(this.url + id, {
            method: "DELETE",
        });
    }
    getEntity(entity) {
        return fetch(this.url + `${entity}`).then((response) => {
            return response.json();
        });
    }
}

const requestObj = new Requests(URL);
requestObj.getEntity("users").then((users) => {
    users.forEach((user) => {
        requestObj.getEntity("posts").then((posts) => {
            console.log(posts);

            posts
                .filter((post) => post.userId === user.id)
                .forEach((post) => {
                    console.log(post);
                    console.log(user);
                    new Card(
                        post.id,
                        post.body,
                        post.title,
                        user.email,
                        user.name,
                    ).render();
                });
        });
    });
});

class Card {
    constructor(id, text, title, email, firstName) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.email = email;
        this.firstName = firstName;
    }

    render() {
        const root = document.querySelector("#root");
        root.insertAdjacentHTML(
            "beforeend",
            `<div id=${this.id}><button>x</button>
    <p>${this.firstName}</p>
    <p>${this.email}</p>
    <h2>${this.title}</h2>
    <p>${this.text}</p>
  </div>`,
        );
        const searchCard = document.getElementById(this.id);
        const button = searchCard.querySelector("button");
        button.onclick = () => this.delete(this.id);
    }

    delete(id) {
        new Requests(Post)
            .delete(id)
            .then((response) => {
                console.log(response);
                if (response.status === 200) {
                    document.getElementById(id).remove();

                } else {
                    throw new Error("Виникла помилка видалення");
                }
            })
            .catch((error) => {
                console.log(error.message);
            });
    }
}