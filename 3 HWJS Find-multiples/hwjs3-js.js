let userNumber = Number(prompt("Please, enter the number!"));
while (Number.isNaN(userNumber)) {
  userNumber = Number(prompt("You haven't entered the number. Please, try again!"));
}

for (let i = 1; i <= userNumber; i++) {
  if (i % 5 === 0 )
    console.log(i);
}
if (userNumber > 0 && userNumber < 5) {
  console.log("Sorry, no numbers");
} 

