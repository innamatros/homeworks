const buttons = document.getElementsByTagName("button");
document.addEventListener("keydown", function (event) {
  for (let button of buttons) {
    button.classList.remove("active")
    if (button.dataset.code === event.key)
      button.classList.add("active")
  }
})


